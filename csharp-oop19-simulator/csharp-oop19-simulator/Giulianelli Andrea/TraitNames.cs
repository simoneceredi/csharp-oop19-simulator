﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Enum with trait Names
    /// </summary>
    public enum TraitNames
    {
        SPEED,
        DIMENSION,
        FOODRADAR,
        TEMPERATURESENSIBILITY,
        CHILDRENQUANTITY
    }
}
