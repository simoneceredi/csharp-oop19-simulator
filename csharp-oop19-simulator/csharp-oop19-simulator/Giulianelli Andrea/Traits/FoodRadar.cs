﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Food radar trait.
    /// </summary>
    public class FoodRadar : AbstractTrait
    {
        private static readonly int MULTIPLIER = 2;
        public FoodRadar(int value) 
            :base(value, (organism) => value * FoodRadar.MULTIPLIER, TraitType.FOODRADAR)
        {
        }
    }
}
