﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Children quantity trait.
    /// </summary>
    public class ChildrenQuantity : AbstractTrait
    {
        private static readonly int MULTIPLIER = 20;
        public ChildrenQuantity(int value) 
            :base(value, (organism) => value * ChildrenQuantity.MULTIPLIER, TraitType.CHILDRENQUANTITY)
        {
        }
    }
}
