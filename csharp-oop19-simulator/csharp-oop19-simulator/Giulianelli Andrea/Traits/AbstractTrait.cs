﻿using System;
using System.Collections.Generic;
using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Abstract Class common to all traits.
    /// </summary>
    public class AbstractTrait : ITrait
    {
        //Declaring the delegate for food consumption.
        public delegate int FoodConsumption(IOrganism organism);
        private readonly FoodConsumption foodConsuption;

        public TraitType Type { get; private set; } //Extend the type property with a private set.
        public int Value { get; private set; } //Extend the value property with a private set.

        protected AbstractTrait(int value, FoodConsumption foodConsuption, TraitType type)
        {
            //Check that parameters aren't null.
            CheckUtil.Check(foodConsuption != null && type != null, new ArgumentNullException("Parameters can't be null"));
            //Check that the value is in the correct range.
            CheckUtil.Check(value >= type.Values.Start && value <= type.Values.Stop, new ArgumentException("Illegal Argument for Trait value"));
            this.Value = value;
            this.foodConsuption = foodConsuption;
            this.Type = type;
        }
        //Use the delegate to get the food consumption of the trait.
        public IEnergy GetFoodConsumption(IOrganism organism)
        {
            return new Energy(this.foodConsuption(organism));
        }

        public override string ToString()
        {
            return "Value: " + this.Value + ", Rarity: " + this.Type.Rarity.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is AbstractTrait trait &&
                   EqualityComparer<TraitType>.Default.Equals(Type, trait.Type) &&
                   Value == trait.Value;
        }

        public override int GetHashCode()
        {
            int hashCode = 1265339359;
            hashCode = hashCode * -1521134295 + EqualityComparer<TraitType>.Default.GetHashCode(Type);
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            return hashCode;
        }
    }
}
