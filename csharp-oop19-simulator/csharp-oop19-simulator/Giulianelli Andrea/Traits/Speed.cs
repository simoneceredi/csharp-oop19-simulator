﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Speed trait.
    /// </summary>
    public class Speed : AbstractTrait
    {
        private static readonly int MULTIPLIER = 5;
        public Speed(int value) 
            :base(value, (organism) => value * Speed.MULTIPLIER, TraitType.SPEED)
        { 
        }
    }
}
