﻿using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using System;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Temperature sensibility trait.
    /// </summary>
    public class TemperatureSensibility : AbstractTrait
    {
        public TemperatureSensibility() 
            :base(0, TemperatureSensibility.Consumption, TraitType.TEMPERATURESENSIBILITY)
        {
        }

        /*
         * It's private static because it's limited to this class and I want to make it usable only within this class.
        */
        private static int Consumption(IOrganism organism)
        {
            int numerator = 200;
            double parameter = 2;
            double environmentTemp = organism.EnvironmentKnowledge.Temperature.Value;
            int organismDim = organism.Traits[TraitType.DIMENSION].Value;
            return (int) (Math.Pow((((double) 1 / 4) * environmentTemp - parameter - ((double) numerator / organismDim)), 2));
        }
    }
}
