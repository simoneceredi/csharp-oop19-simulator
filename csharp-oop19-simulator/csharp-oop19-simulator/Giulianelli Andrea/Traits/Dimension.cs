﻿using System;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Dimension trait.
    /// </summary>
    public class Dimension : AbstractTrait
    {
        private static readonly double MULTIPLIER = 0.1;
        public Dimension(int value) 
            :base(value, (organism) => Convert.ToInt32(value * Dimension.MULTIPLIER), TraitType.DIMENSION)
        {
        }
    }
}
