using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// It's the Interface that describe a trait.
    /// </summary>
    public interface ITrait
    {
        /// <summary>Value of the trait.</summary>
        int Value { get; }
        /// <summary>Type of the trait. </summary>
        TraitType Type { get; }
        /// <summary>GetFoodConsumption</summary>
        /// <param name="organism">organism where the trait is applied</param>
        /// <returns>The food consumption of the trait</returns>
        IEnergy GetFoodConsumption(IOrganism organism);
    }
}
