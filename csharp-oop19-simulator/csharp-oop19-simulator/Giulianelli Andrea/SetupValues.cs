﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Describe trait's range values.
    /// In Java was an enum, this is the representation that fits more my needs.
    /// </summary>
    public class SetupValues
    {
        public static readonly SetupValues DIMENSION = new SetupValues(50, 200, 100);
        public static readonly SetupValues SPEED = new SetupValues(1, 5, 2);
        public static readonly SetupValues INITIALQUANTITY = new SetupValues(50, 150, 50);
        public static readonly SetupValues FOODQUANTITY = new SetupValues(200, 2000, 1600);
        public static readonly SetupValues FOODVARIATION = new SetupValues(-5, 5, 0);
        public static readonly SetupValues CHILDRENQUANTITY = new SetupValues(1, 3, 1);
        public static readonly SetupValues FOODRADAR = new SetupValues(1, 3, 1);
        public static readonly SetupValues TEMPERATURE = new SetupValues(5, 30, 20);
        public static readonly SetupValues TEMPERATURESENSIBILITY = new SetupValues(0, 0, 0);

        public int Start { get; private set; }
        public int Stop { get; private set; }
        public int DefaultValue { get; private set; }

        private SetupValues(int start, int stop, int defaultValue)
        {
            this.Start = start;
            this.Stop = stop;
            this.DefaultValue = defaultValue;
        }
    }
}
