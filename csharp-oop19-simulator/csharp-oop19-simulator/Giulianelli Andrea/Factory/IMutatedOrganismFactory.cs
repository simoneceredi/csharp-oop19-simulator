﻿using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;

namespace csharp_oop19_simulator.Giulianelli_Andrea.Factory
{
    /// <summary>
    /// Interface of mutated children factory.
    /// </summary>
    public interface IMutatedOrganismFactory
    {

        /// <summary>
        /// Children is created based on dad's traits value.
        /// </summary>
        /// <param name="organism">Dad organism</param>
        /// <returns>The children created</returns>
        IOrganism CreateMutated(IOrganism organism);
    }
}
