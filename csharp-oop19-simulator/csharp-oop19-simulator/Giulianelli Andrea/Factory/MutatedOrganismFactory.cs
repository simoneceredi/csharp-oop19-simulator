﻿using System;
using System.Collections.Generic;
using System.Linq;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;

namespace csharp_oop19_simulator.Giulianelli_Andrea.Factory
{
    /// <summary>
    /// Children Factory.
    /// </summary>
    public class MutatedOrganismFactory : IMutatedOrganismFactory
    {
        public IOrganism CreateMutated(IOrganism organism)
        {
            CheckUtil.Check(organism != null, new ArgumentNullException("Parameter can't be null"));
            Dictionary<TraitType, ITrait> traits = organism.Traits;
            Dictionary<TraitType, ITrait> mutatedTraits = traits
                                                         .Where(entrySet => !entrySet.Value.Type.Rarity.Equals(MutationRarity.NOMUTATION))
                                                         .ToDictionary(entrySet => entrySet.Key, entrySet => this.GetMutatedTrait(entrySet.Key, entrySet.Value.Value));
            //Children has the same energy of dad
            IOrganismBuilder organismBuilder = new OrganismBuilder(organism.Energy)
                                                .SetEnvironmentKnowledge(organism.EnvironmentKnowledge);
            //Insert mutate trait.
            mutatedTraits.ToList().ForEach(entrySet => organismBuilder.SetTrait(entrySet.Value));
            //Insert also not mutable trait in children
            traits.Where(entrySet => entrySet.Value.Type.Rarity.Equals(MutationRarity.NOMUTATION))
                .ToList().ForEach(entrySet => organismBuilder.SetTrait(entrySet.Value));
            //return the built organism.
            return organismBuilder.Build();
        }

        private ITrait GetMutatedTrait(TraitType type, int value)
        {
            CheckUtil.Check(type != null, new ArgumentNullException("Parameter can't be null"));
            ITrait newTrait;
            Random rnd = new Random();
            int newValue = value;
            if(rnd.NextDouble() < type.Rarity.GetPercentage())
            {
                double variationPercentage = rnd.NextDouble() / (value > 1 ? 2 : 1);
                //If the random number is lower than the rarity percentage, the trait mutate.
                do
                {
                    //0-flase 1-true
                    if(rnd.Next(2) == 1)
                    {
                        //If true the sign is positive.
                        newValue = Convert.ToInt32(Math.Round(value + value * variationPercentage));   
                    }
                    else
                    { 
                        //If false the sign is negative.
                        newValue = Convert.ToInt32(Math.Round(value - value * variationPercentage));
                    }
                } while (newValue < type.Values.Start || newValue > type.Values.Stop);
            }

            switch(type.Name)
            {
                case TraitNames.SPEED:
                    newTrait = new Speed(newValue);
                    break;
                case TraitNames.DIMENSION:
                    newTrait = new Dimension(newValue);
                    break;
                case TraitNames.CHILDRENQUANTITY:
                    newTrait = new ChildrenQuantity(newValue);
                    break;
                case TraitNames.FOODRADAR:
                    newTrait = new FoodRadar(newValue);
                    break;
                default:
                    /* If no one can handle the request instead of returning null, 
                    *  it throw a runtime exception. */
                    throw new ArgumentException();
            }

            return newTrait;
        }
    }
}
