﻿namespace csharp_oop19_simulators.Giulianelli_Andrea
{
    /// <summary>
    /// Enum that describe if a trait affect Movement and so we have to consider the consumption for the movement
    /// </summary>
    public enum AffectMovement
    {
        YES,
        NO
    }
}
