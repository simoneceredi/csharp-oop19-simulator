﻿using System;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Utility class with some controls.
    /// </summary>
    public static class CheckUtil
    {
        /// <summary>
        /// Check the supplier, if it's false throw the exception passed as argument.
        /// </summary>
        /// <param name="supplier">The bool condition to verify</param>
        /// <param name="excpetion">The exception to throw if the condition isn't verified.</param>
        public static void Check(bool supplier, Exception excpetion)
        {
            if(!supplier)
            {
                throw excpetion;
            }
        }
    }
}
