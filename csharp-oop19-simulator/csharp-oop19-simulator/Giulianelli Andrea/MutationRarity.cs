﻿namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// Describe the Mutation Rarity of a trait.
    /// In Java was an enum, this is the representation that fits more my needs.
    /// </summary>
    public class MutationRarity
    {
        public static readonly MutationRarity NOMUTATION = new MutationRarity(0,"No Mutation");
        public static readonly MutationRarity VERYRARE = new MutationRarity(0.05, "Very Rare");
        public static readonly MutationRarity RARE = new MutationRarity(0.20, "Rare");
        public static readonly MutationRarity NORMAL = new MutationRarity(0.50, "Normal");
        public static readonly MutationRarity COMMON = new MutationRarity(0.70, "Common");
        public static readonly MutationRarity VERYCOMMON = new MutationRarity(0.90, "Very Common");

        private readonly double perc;
        private readonly string name;
        private MutationRarity(double perc, string name)
        {
            this.perc = perc;
            this.name = name;
        }

        /// <summary>GetPercentage of Mutation.</summary>
        /// <returns>The percentage</returns>
        public double GetPercentage()
        {
            return this.perc;
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
