﻿using csharp_oop19_simulators.Giulianelli_Andrea;

namespace csharp_oop19_simulator.Giulianelli_Andrea
{
    /// <summary>
    /// This class describe all types of traits.
    /// In Java was an enum, this is the representation that fits more my needs.
    /// </summary>
    public class TraitType
    {
        public static readonly TraitType SPEED = new TraitType(AffectMovement.YES, MutationRarity.COMMON, SetupValues.SPEED, TraitNames.SPEED);
        public static readonly TraitType DIMENSION = new TraitType(AffectMovement.YES, MutationRarity.NORMAL, SetupValues.DIMENSION, TraitNames.DIMENSION);
        public static readonly TraitType FOODRADAR = new TraitType(AffectMovement.YES, MutationRarity.VERYRARE, SetupValues.FOODRADAR, TraitNames.FOODRADAR);
        public static readonly TraitType TEMPERATURESENSIBILITY = new TraitType(AffectMovement.YES, MutationRarity.NOMUTATION, SetupValues.TEMPERATURESENSIBILITY, TraitNames.TEMPERATURESENSIBILITY);
        public static readonly TraitType CHILDRENQUANTITY = new TraitType(AffectMovement.NO, MutationRarity.RARE, SetupValues.CHILDRENQUANTITY, TraitNames.CHILDRENQUANTITY);

        private AffectMovement AffectM { get; set; }
        public MutationRarity Rarity { get; private set; }
        public SetupValues Values { get; private set; }
        public TraitNames Name { get; private set; }
        private TraitType(AffectMovement affectMovement, MutationRarity rarity, SetupValues values, TraitNames name)
        {
            this.AffectM = affectMovement;
            this.Rarity = rarity;
            this.Values = values;
            this.Name = name;
        }

        /// <summary>IsAffectMovement</summary>
        /// <returns>True if affect movement</returns>
        public bool IsAffectMovement()
        {
            return this.AffectM.Equals(AffectMovement.YES);
        }

        public override string ToString()
        {
            return this.Name.ToString();
        }
    }
}
