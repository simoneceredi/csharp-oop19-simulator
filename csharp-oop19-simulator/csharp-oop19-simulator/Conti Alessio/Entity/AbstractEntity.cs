﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity
{
    /// <summary>
    /// Abstract class to describe entities.
    /// </summary>
    public abstract class AbstractEntity : IEntity
    {
        public AbstractEntity(Energy energy)
        {
            this.Energy = energy;
        }

        public Energy Energy { get; set; }

        public override string ToString()
        {
            return "Entity= [energy=" + Energy + "]";
        }
    }
}
