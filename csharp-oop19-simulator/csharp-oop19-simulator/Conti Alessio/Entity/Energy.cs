﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity
{
    /// <summary>
    /// Represents an energy level.
    /// </summary>
    public class Energy : IEnergy
    {
        public int EnergyLevel { get; set; }

        public Energy(int energy)
        {
            EnergyLevel = energy;
        }

        /// <summary>Add two energy values.</summary>
        /// <param name="a">first elemnt of the operation.</param>
        /// <param name="b">second elemnt of the operation.</param>
        /// <returns>A new Energy object.</returns>
        public static Energy operator +(Energy a, Energy b)
        {
            return new Energy(a.EnergyLevel + b.EnergyLevel);
        }

        /// <summary>Detract the second Energy to the first.</summary>
        /// <param name="a">first elemnt of the operation.</param>
        /// <param name="b">second elemnt of the operation.</param>
        /// <returns>A new Energy object.</returns>
        public static Energy operator -(Energy a, Energy b)
        {
            return new Energy(a.EnergyLevel - b.EnergyLevel);
        }

        /// <summary>Compare if the first value is greater or equal than the second.</summary>
        /// <param name="a">first elemnt of the operation.</param>
        /// <param name="b">second elemnt of the operation.</param>
        /// <returns>True if the firts value is greater or equal than the second.</returns>
        public static bool operator >=(Energy a, Energy b)
        {
            return a.EnergyLevel >= b.EnergyLevel;
        }

        /// <summary>Compare if the first value is less or equal than the second.</summary>
        /// <param name="a">first elemnt of the operation.</param>
        /// <param name="b">second elemnt of the operation.</param>
        /// <returns>True if the firts value is greater or equal than the second.</returns>
        public static bool operator <=(Energy a, Energy b)
        {
            return a.EnergyLevel <= b.EnergyLevel;
        }

        public override bool Equals(object obj)
        {
            return obj is Energy energy &&
                   EnergyLevel == energy.EnergyLevel;
        }

        public override int GetHashCode()
        {
            return -948749259 + EnergyLevel.GetHashCode();
        }
    }
}
