﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity
{
    /// <summary>
    /// Represents an energy level.
    /// </summary>
    public interface IEnergy
    {
        /// <summary>Represents an energy level.</summary>
        int EnergyLevel { get; set; }
    }
}
