﻿using System;

namespace csharp_oop19_simulator.Conti_Alessio.Entity.Foods
{
    /// <summary>
    /// Implementation of Food.
    /// </summary>
    public class Food : AbstractEntity
    {
        private const string exceptionMessage = "Operation not supported for type Food.";

        /// <param name="energy">the current Food energy.</param>
        public Food(Energy energy) : base(energy)
        {
        }

        /// <param name="energy">the Food energy.</param>
        /// <exception cref="NotSupportedException">Not implemented method.</exception>
        public void SetEnergy(Energy energy) => throw new NotSupportedException(exceptionMessage);

        public override string ToString()
        {
            return "Food= [energy=" + Energy + "]";
        }
    }
}
