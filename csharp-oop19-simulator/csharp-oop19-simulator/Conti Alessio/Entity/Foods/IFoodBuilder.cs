﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity.Foods
{
    /// <summary>
    /// Interface that models a FoodBuilder.
    /// </summary>
    public interface IFoodBuilder
    {
        /// <param name="energy">the food energy value.</param>
        /// <returns>the current FoodBuilder.</returns>
        IFoodBuilder SetEnergy(Energy energy);

        /// <returns>an instance of OrganismImpl if all the fields are not null.</returns>
        /// <exception cref="IllegalArgumentException">IllegalArgumentException if some the fields are null.</exception>
        Food Build();
    }
}
