﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity.Foods
{
    /// <summary>
    /// Implementation of IFoodBuilder.
    /// </summary>
    public class FoodBuilder : IFoodBuilder
    {
        private const int defaultEnergy = 100;
        private readonly Energy energyValue = new Energy(defaultEnergy);
        private Energy energy;

        /// <summary>Creates a new foodBuilder.</summary>
        public FoodBuilder()
        {
            energy = energyValue;
        }

        public IFoodBuilder SetEnergy(Energy energy)
        {
            this.energy = energy;
            return this;
        }

        public Food Build()
        {
            return new Food(this.energy);
        }
    }
}
