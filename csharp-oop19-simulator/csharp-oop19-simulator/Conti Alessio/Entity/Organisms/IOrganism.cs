﻿using csharp_oop19_simulator.Giulianelli_Andrea;
using csharp_oop19_simulator.Ceredi_Simone;
using System.Collections.Generic;

namespace csharp_oop19_simulator.Conti_Alessio.Entity.Organisms
{
    /// <summary>
    /// Interface that defines an Organism.
    /// </summary>
    public interface IOrganism : IEntity
    {
        /// <returns>A Dictionary containing all organism's traits.</returns>
        Dictionary<TraitType, ITrait> Traits { get; }

        /// <returns>An IOrganismEnvironmentHolder containing all the information that an organism knows about the environment.</returns>
        IOrganismEnvironmentHolder EnvironmentKnowledge { get; }
    }
}
