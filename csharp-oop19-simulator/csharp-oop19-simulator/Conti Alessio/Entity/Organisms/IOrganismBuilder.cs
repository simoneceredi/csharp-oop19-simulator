﻿using csharp_oop19_simulator.Giulianelli_Andrea;
using csharp_oop19_simulator.Ceredi_Simone;

namespace csharp_oop19_simulator.Conti_Alessio.Entity.Organisms
{
    /// <summary>
    /// Interface that models an Organisms builder.
    /// </summary>
    public interface IOrganismBuilder
    {
        /// <param name="type">the type of mutation.</param>
        /// <param name="trait">the proper trait that must be associated to the organism.</param>
        /// <returns>An OrganismBuilder.</returns>
        IOrganismBuilder SetTrait(ITrait trait);

        /// <param name="environmentKnowledge">organism knowledges an organism has about the environment.</param>
        /// <returns>An OrganismBuilder.</returns>
        IOrganismBuilder SetEnvironmentKnowledge(IOrganismEnvironmentHolder environmentKnowledge);

        /// <returns>An instance of OrganismImpl if all the fields are not null.</returns>
        Organism Build();
    }
}
