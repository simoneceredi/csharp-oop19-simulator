﻿using csharp_oop19_simulator.Giulianelli_Andrea;
using System;
using System.Collections.Generic;
using csharp_oop19_simulator.Ceredi_Simone;

namespace csharp_oop19_simulator.Conti_Alessio.Entity.Organisms
{
    /// <summary>
    /// Implementation of the organism builder.
    /// </summary>
    public class OrganismBuilder : IOrganismBuilder
    {
        private const string exceptionMessage = "Argument can not be null.";

        private readonly Energy energy;
        private readonly Dictionary<TraitType, ITrait> traits;
        private IOrganismEnvironmentHolder environmentKnowledge = null;

        /// <param name="energy">organism energy.</param>
        public OrganismBuilder(Energy energy)
        {
            this.energy = energy;
            this.traits = new Dictionary<TraitType, ITrait>();
        }

        public IOrganismBuilder SetTrait(ITrait trait)
        {
            traits.Add(trait.Type, trait);
            return this;
        }

        public IOrganismBuilder SetEnvironmentKnowledge(IOrganismEnvironmentHolder environmentKnowledge)
        {
            this.environmentKnowledge = environmentKnowledge;
            return this;
        }

        public Organism Build()
        {
            if (energy != null && traits.Count!=0 && environmentKnowledge!=null)
            {
                return new Organism(new Energy(energy.EnergyLevel), traits, environmentKnowledge);
            }
            throw new InvalidOperationException(exceptionMessage);
        }
    }
}
