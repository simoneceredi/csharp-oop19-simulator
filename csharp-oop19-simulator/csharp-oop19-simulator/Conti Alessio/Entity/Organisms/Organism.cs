﻿using csharp_oop19_simulator.Giulianelli_Andrea;
using csharp_oop19_simulator.Ceredi_Simone;
using System.Collections.Generic;

namespace csharp_oop19_simulator.Conti_Alessio.Entity.Organisms
{
    /// <summary>
    /// Class that models an organism.
    /// </summary>
    public class Organism : AbstractEntity, IOrganism
    {
        /// <param name="energy">organism energy.</param>
        /// <param name="traits">organism traits.</param>
        /// <param name="environmentKnowledge">organism knowledges about the environment.</param>
        public Organism(Energy energy, Dictionary<TraitType, ITrait> traits, IOrganismEnvironmentHolder environmentKnowledge) : base(energy)
        {
            this.Traits = traits;
            this.EnvironmentKnowledge = environmentKnowledge;
        }

        public Dictionary<TraitType, ITrait> Traits { get; }

        public IOrganismEnvironmentHolder EnvironmentKnowledge { get; }

        public override string ToString()
        {
            return "Organism= [energy=" + Energy + ", traits=" + Traits + "]";
        }
    }
}
