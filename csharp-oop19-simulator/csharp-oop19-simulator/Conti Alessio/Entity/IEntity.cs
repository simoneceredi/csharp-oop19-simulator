﻿namespace csharp_oop19_simulator.Conti_Alessio.Entity
{
    /// <summary>
    /// Interface that defines an entity which can populate the environment.
    /// </summary>
    public interface IEntity
    {
        /// <summary>Represents the entity energy.</summary>
        Energy Energy { get; set; }
    }
}
