﻿using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using System.Collections.Generic;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Represents an advanced environment.
    /// </summary>
    public interface IAdvancendEnvironment : IBasicEnvironment, IOrganismEnvironmentHolder
    {
        /// <summary>
        /// Returns the foods that the organism can see.
        /// </summary>
        /// <param name="organism">the organism</param>
        /// <returns>a set of food containing the food nearby the given organism</returns>
        ISet<Food> GetNearbyFoods(IOrganism organism);
    }
}
