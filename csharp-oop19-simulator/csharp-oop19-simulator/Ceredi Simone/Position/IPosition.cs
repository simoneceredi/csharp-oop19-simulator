﻿namespace csharp_oop19_simulator.Ceredi_Simone.Position
{
    /// <summary>
    /// Represents a 2 dimension position.
    /// </summary>
    public interface IPosition
    {
        /// <summary>
        /// the X value of the position
        /// </summary>
        double X { set; get; }

        /// <summary>
        /// the Y value of the position
        /// </summary>
        double Y { set; get; }

    }
}
