﻿using System;

namespace csharp_oop19_simulator.Ceredi_Simone.Position
{
    /// <summary>
    /// Implementation of a 2d position.
    /// </summary>
    class Position : IPosition
    {
        public double X { set; get; }
        public double Y { set; get; }

        /// <summary>
        /// Creates a new Position
        /// </summary>
        /// <param name="x">the x value of the position to set.</param>
        /// <param name="y">the y value of the position to set.</param>
        public Position(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return String.Format("X={0}, Y={1}", X, Y);
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   X == position.X &&
                   Y == position.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
