﻿using System;
using System.Runtime.Serialization;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Represents an exception that tells that an organism went out from the environment.
    /// </summary>
    [Serializable]
    public class OutOfEnvironmentException : Exception
    {
        private const string EXCEPTIONMESSAGE = "Border reached.";
        public OutOfEnvironmentException()
        {
            Console.Error.WriteLine(EXCEPTIONMESSAGE);
        }

        public OutOfEnvironmentException(string message) : base(message)
        {
        }

        public OutOfEnvironmentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OutOfEnvironmentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}