﻿using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using System;
using System.Collections.Generic;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /*
     * Represent the environment in which Organisms and Food are contained.
     */
    public interface IEnvironment
    {
        /// <returns>the number of organisms inside the Environment</returns>
        int CurrentOrganismQuantity { get; }

        /// <returns>the quantity of food inside the Environment</returns>
        int CurrentFoodQuantity { get; }

        /// <summary>
        /// Add an organism to the environment.
        /// </summary>
        /// <param name="organism">the organism to add</param>
        void AddOrganism(IOrganism organism);

        /// <summary>
        /// Add an organism in the father position.
        /// </summary>
        /// <param name="father">the father organism</param>
        /// <param name="son">the new organisms</param>
        /// <exception cref="InvalidOperationException">if the father doesn't exists</exception>
        void AddOrganism(IOrganism father, IOrganism son);

        /// <summary>
        /// Add a piece of food to the environment.
        /// </summary>
        /// <param name="food">the food piece to add.</param>
        void AddFood(Food food);

        /// <summary>
        /// Move an organism to a new position.
        /// </summary>
        /// <param name="organism">the organism to move</param>
        /// <param name="xOffset">the offset of the x-axis</param>
        /// <param name="yOffset">the offset of the y-axis</param>
        /// <exception cref="OutOfEnvironmentException">if the Organism tries to exit the Environment</exception>
        /// <exception cref="InvalidOperationException">if the organism doesn't exists</exception>
        void MoveOrganism(IOrganism organism, int xOffset, int yOffset);

        /// <summary>
        /// Removes an organism from the environment.
        /// </summary>
        /// <param name="organism">the organism to remove</param>
        void RemoveOrganism(IOrganism organism);

        /// <summary>
        /// Removes a food from the environment.
        /// </summary>
        /// <param name="food">the food piece to remove</param>
        void RemoveFood(Food food);

        /// <returns>one by one the organisms inside the environment</returns>
        IEnumerable<IOrganism> GetOrganisms();

        /// <param name="organism">the organism that requires the food piece</param>
        /// <returns>a nullable containing the food or an empty one if the position was empty</returns>
        Food GetFood(IOrganism organism);

        /// <param name="position">the position of the food required</param>
        /// <returns>a nullable containing the food or an empty one if the position was empty</returns>
        Food GetFood(IPosition position);

        // TODO use {get;}
        /// <returns>a tuple of each Food and its position in the environment</returns>
        ISet<Tuple<IPosition, Food>> GetPositionFoods();

        // TODO use {get;}
        /// <returns>a tuple of each Organism and its position in the environment</returns>
        ISet<Tuple<IPosition, IOrganism>> GetPositionOrganisms();

        // TODO use {get;}
        /// <returns>the environment dimension</returns>
        IPosition Dimension { get; }

        /// <summary>
        /// Removes every food piece from the environment.
        /// </summary>
        void Clear();
    }
}
