﻿using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using System;
using System.Collections.Generic;
using System.Linq;
using csharp_oop19_simulator.Conti_Alessio.Entity;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Abstract implementation of an environment.
    /// </summary>
    public abstract class AbstractEnvironment : IEnvironment
    {
        private static readonly Random RND = new Random();
        protected readonly IDictionary<IPosition, Food> Foods = new Dictionary<IPosition, Food>();

        protected readonly IDictionary<IOrganism, IPosition> Organisms = new Dictionary<IOrganism, IPosition>();

        public int CurrentOrganismQuantity => Organisms.Count;

        public int CurrentFoodQuantity => Foods.Count;

        public IPosition Dimension { set; get; }

        public AbstractEnvironment(int xDimension, int yDimension)
        {
            Dimension = new csharp_oop19_simulator.Ceredi_Simone.Position.Position(xDimension, yDimension);
        }

        public void AddFood(Food food)
        {
            if (food == null)
            {
                throw new ArgumentNullException();
            }
            Position.Position foodPosition = GetRandomPosition();
            
            if (Foods.ContainsKey(foodPosition))
            {
                Food f = Foods[foodPosition];
                Foods.Remove(foodPosition);
                f.Energy += new FoodBuilder().Build().Energy;
                Foods[foodPosition] = f;
            }
            else
            {
                Foods[foodPosition] = food;
            }
            

        }

        private csharp_oop19_simulator.Ceredi_Simone.Position.Position GetRandomPosition()
        {
            return new csharp_oop19_simulator.Ceredi_Simone.Position.Position(AbstractEnvironment.RND.Next(0, int.Parse(Dimension.X.ToString())), AbstractEnvironment.RND.Next(0, int.Parse(Dimension.Y.ToString())));
        }

        public void AddOrganism(IOrganism organism)
        {
            if (organism == null)
            {
                throw new ArgumentNullException();
            }
            IPosition organismPosition = GetRandomPosition();
            Organisms[organism] = organismPosition;
        }

        public void AddOrganism(IOrganism father, IOrganism son)
        {
            if (father == null || son == null)
            {
                throw new ArgumentNullException();
            }
            IPosition pos = Organisms[father];
            Organisms[son] = pos;
        }

        public void Clear()
        {
            Foods.Clear();
        }

        public Food GetFood(IOrganism organism)
        {
            return Foods[Organisms[organism]];
        }

        public Food GetFood(IPosition position)
        {
            return Foods[position];
        }

        public IEnumerable<IOrganism> GetOrganisms()
        {
            ISet<IOrganism> o = new HashSet<IOrganism>(Organisms.Keys);
            foreach (IOrganism org in o)
            {
                yield return org;
            }
        }

        public ISet<Tuple<IPosition, Food>> GetPositionFoods()
        {
            return new HashSet<Tuple<IPosition, Food>>(Foods.Select(o => Tuple.Create(o.Key, o.Value)));
        }

        public ISet<Tuple<IPosition, IOrganism>> GetPositionOrganisms()
        {
            return new HashSet<Tuple<IPosition, IOrganism>>(Organisms.Select(o => Tuple.Create(o.Value, o.Key)));
        }

        public void MoveOrganism(IOrganism organism, int xOffset, int yOffset)
        {
            IPosition p = Organisms[organism];
            IPosition newPos = new csharp_oop19_simulator.Ceredi_Simone.Position.Position(p.X + xOffset, p.Y + yOffset);
            CheckPosition(newPos);
            Organisms[organism] = newPos;
        }

        /// <summary>
        /// Template method that checks if a given position is valid.
        /// </summary>
        /// <param name="pos">the position that will be checked</param>
        /// <returns>true if the position is valid, false if it's not valid. 
        /// It will throw a OutOfEnvironment exception if the position is outside the environment boundaries</returns>
        protected abstract bool CheckPosition(IPosition pos);
        

        public void RemoveFood(Food food)
        {
            if(food == null)
            {
                throw new ArgumentNullException();
            }
            Foods.Remove(Foods.FirstOrDefault(x => x.Value.Equals(food)).Key);
        }

        public void RemoveOrganism(IOrganism organism)
        {
            if (organism.Equals(null))
            {
                throw new ArgumentNullException();
            }
            Organisms.Remove(organism);
        }

        public override string ToString()
        {
            return string.Format(base.ToString() + " AbstractEnvironment [Dimension={0}, CurrentFoodQuantity={1}, CurrentOrganismQuantity={2}, Foods={3}, Organisms={4}",
                Dimension.ToString(), CurrentFoodQuantity, CurrentOrganismQuantity, Foods.ToString(), Organisms.ToString());
        }
    }
}
