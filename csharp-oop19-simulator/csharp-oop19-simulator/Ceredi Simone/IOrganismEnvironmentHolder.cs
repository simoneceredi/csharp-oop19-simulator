﻿using csharp_oop19_simulator.Ceredi_Simone.Position;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// An environment interface created for the organism.
    /// </summary>
    public interface IOrganismEnvironmentHolder
    {
        /// <summary>
        /// Returns the temperature.
        /// </summary>
        /// <returns>the current environment temperature</returns>
        Temperature Temperature { get; }

        /// <summary>
        /// allows organism to get the environment holder.
        /// </summary>
        /// <returns>The organism holder</returns>
        IOrganismEnvironmentHolder OrganismEnvironmentHolder { get; }
    }
}
