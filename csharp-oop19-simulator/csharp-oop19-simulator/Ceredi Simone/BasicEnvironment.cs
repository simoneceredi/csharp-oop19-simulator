﻿using csharp_oop19_simulator.Ceredi_Simone.Position;
using System;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Basic implementation for an Environment.
    /// </summary>
    public class BasicEnvironment : AbstractEnvironment, IBasicEnvironment
    {
        private readonly int dailyFoodQuantityModification;
        public int MorningFoodQuantity { get; private set; }

        public BasicEnvironment(int xDimension, int yDimension, int morningFoodQuantity, int dailyFoodQuantityModification) : base(xDimension, yDimension)
        { 
            MorningFoodQuantity = morningFoodQuantity;
            this.dailyFoodQuantityModification = dailyFoodQuantityModification;
        }

        public void NextDay()
        {
            base.Clear();
            MorningFoodQuantity += dailyFoodQuantityModification;
        }

        public override string ToString()
        {
            return String.Format(base.ToString() + "BasicEnvironment [MorningFoodQuantity={0}, dailyFoodQuantityModification={1}]",
                MorningFoodQuantity, dailyFoodQuantityModification);
        }

        protected override bool CheckPosition(IPosition pos)
        {
            if (pos.X < 0 || pos.X > Dimension.X || pos.Y < 0 || pos.Y > Dimension.Y)
            {
                throw new OutOfEnvironmentException();
            }
            return true;
        }
    }
}
