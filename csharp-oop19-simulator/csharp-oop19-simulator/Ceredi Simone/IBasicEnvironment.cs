﻿namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Represents a basic environment.
    /// </summary>
    public interface IBasicEnvironment : IEnvironment
    { 
        /// <summary>
        /// Tells the Environment that it's a new day.
        /// </summary>
        void NextDay();
    }
}
