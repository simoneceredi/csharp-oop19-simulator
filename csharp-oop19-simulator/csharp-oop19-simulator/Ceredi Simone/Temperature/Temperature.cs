﻿
namespace csharp_oop19_simulator.Ceredi_Simone.Position
{
    /// <summary>
    /// The implementation of the Environment Temperature.
    /// </summary>
    public class Temperature
    {
        public double Value { set; get; }

        public Temperature(double value)
        {
            Value = value;
        }
    }
}
