﻿using csharp_oop19_simulator.Ceredi_Simone.Position;
using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Giulianelli_Andrea;
using System.Collections.Generic;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Implementation for an IAdvancedEnvironment.
    /// </summary>
    public class AdvancedEnvironment : BasicEnvironment, IAdvancendEnvironment
    {
        public Temperature Temperature { get; private set; }

        public IOrganismEnvironmentHolder OrganismEnvironmentHolder => this;

        public AdvancedEnvironment(int xDimension, int yDimension, int morningFoodQuantity,
            int dailyFoodQuantityModification, Temperature temperature) : 
            base(xDimension, yDimension, morningFoodQuantity, dailyFoodQuantityModification)
        {
            Temperature = temperature;
        }

        public ISet<Food> GetNearbyFoods(IOrganism organism)
        {
            return GetFoods(organism);
        }

        private ISet<Food> GetFoods(IOrganism organism)
        {
            ISet<Food> ret = new HashSet<Food>();
            IPosition p = Organisms[organism];
            int radius = organism.Traits[TraitType.FOODRADAR].Value - 1;
            for(int i = -radius; i <= radius; i++)
            {
                for (int j = -radius; j < radius; j++)
                {
                    Food f = Foods[new csharp_oop19_simulator.Ceredi_Simone.Position.Position(p.X + i, p.Y + j)];
                    if (!f.Equals(null))
                    {
                        ret.Add(f);
                    }
                }
            }
            return ret;
        }
    }
}
