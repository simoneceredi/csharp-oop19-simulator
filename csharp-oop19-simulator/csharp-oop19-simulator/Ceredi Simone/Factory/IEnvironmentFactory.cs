﻿using csharp_oop19_simulator.Ceredi_Simone.Position;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// Models a factory for the environment.
    /// </summary>
    public interface IEnvironmentFactory
    {
        /// <summary>
        /// Creates a BasicEnvironment
        /// </summary>
        /// <param name="width">the environment width</param>
        /// <param name="height">the environment height</param>
        /// <param name="morningFoodQuantity">the food quantity for the first morning</param>
        /// <param name="dailyFoodQuantityModification">how much will the quantity of food be modified every morning</param>
        /// <returns>a BasicEnvironment</returns>
        IBasicEnvironment CreateBasicEnvironmnet(int width, int height, int morningFoodQuantity, int dailyFoodQuantityModification);

        /// <summary>
        /// Creates an AdvancedEnvironment.
        /// </summary>
        /// <param name="width">the environment width</param>
        /// <param name="height">the environment height</param>
        /// <param name="morningFoodQuantity">the food quantity for the first morning</param>
        /// <param name="dailyFoodQuantityModification">how much will the quantity of food be modified every morning</param>
        /// <param name="temperature">the initial temperature of the environment</param>
        /// <returns>an AdvancedEnvironment</returns>
        IAdvancendEnvironment CreateAdvancedEnvironmnet(int width, int height, int morningFoodQuantity, int dailyFoodQuantityModification, Temperature temperature);
    }
}
