﻿using csharp_oop19_simulator.Ceredi_Simone.Position;

namespace csharp_oop19_simulator.Ceredi_Simone
{
    /// <summary>
    /// IEnvironmentFactory implementation.
    /// </summary>
    public class EnvironmentFactory : IEnvironmentFactory
    {
        public IAdvancendEnvironment CreateAdvancedEnvironmnet(int width, int height, int morningFoodQuantity, int dailyFoodQuantityModification, Temperature temperature)
        {
            return new AdvancedEnvironment(width, height, morningFoodQuantity, dailyFoodQuantityModification, temperature);
        }

        public IBasicEnvironment CreateBasicEnvironmnet(int width, int height, int morningFoodQuantity, int dailyFoodQuantityModification)
        {
            return new BasicEnvironment(width, height, morningFoodQuantity, dailyFoodQuantityModification);
        }
    }
}
