﻿using csharp_oop19_simulator.Conti_Alessio.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Conti_Alessio
{
    [TestClass]
    public class EnergyTest
    {
        [TestMethod]
        public void TestValue()
        {
            Energy initialEnergy = new Energy(100);
            Assert.AreEqual(new Energy(100), initialEnergy);

            initialEnergy += new Energy(10);
            Assert.AreEqual(new Energy(110), initialEnergy);

            initialEnergy -= new Energy(10);
            Assert.AreEqual(new Energy(100), initialEnergy);

            Assert.IsTrue(new Energy(100) >= initialEnergy);
        }
    }
}
