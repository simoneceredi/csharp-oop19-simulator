﻿using System;
using System.Collections.Generic;
using System.Linq;
using csharp_oop19_simulator.Ceredi_Simone;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Giulianelli_Andrea;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Conti_Alessio
{
    [TestClass]
    public class OrganismTest
    {
        [TestMethod]
        public void TestOrganismBuilder()
        {
            Energy initialEnergy = new Energy(100);
            // Factory to create an Enviroment
            IEnvironmentFactory factory = new EnvironmentFactory();
            AdvancedEnvironment environment = (AdvancedEnvironment)factory.CreateAdvancedEnvironmnet(100, 100, 10, 0, new Temperature(20));
            // Organism's traits
            ITrait speed = new Speed(3);
            ITrait dimension = new Dimension(50);
            ITrait foodradar = new FoodRadar(3);
            ITrait temperatureSenibility = new TemperatureSensibility();
            ITrait childrenQuantity = new ChildrenQuantity(3);
            // Set up the Builder
            IOrganismBuilder organismBuilder = new OrganismBuilder(initialEnergy);
            organismBuilder.SetTrait(speed);
            organismBuilder.SetTrait(dimension);
            organismBuilder.SetTrait(foodradar);
            organismBuilder.SetTrait(temperatureSenibility);
            organismBuilder.SetTrait(childrenQuantity);
            organismBuilder.SetEnvironmentKnowledge(environment);
            // Add Organism to the Environment
            environment.AddOrganism(organismBuilder.Build());
            IOrganism organism = environment.GetOrganisms().ElementAt(0);
            Assert.AreEqual(initialEnergy, organism.Energy);
            // Set up Traits that shoul be in the organism
            Dictionary<TraitType, ITrait> expectedTraits = new Dictionary<TraitType, ITrait>
            {
                { TraitType.SPEED, speed },
                { TraitType.DIMENSION, dimension },
                { TraitType.FOODRADAR, foodradar },
                { TraitType.TEMPERATURESENSIBILITY, temperatureSenibility },
                { TraitType.CHILDRENQUANTITY, childrenQuantity }
            };
            Assert.AreEqual(expectedTraits.Count, organism.Traits.Count);
        }

        [TestMethod]
        public void TestExceptionInOrganismBuild()
        {
            Energy initialEnergy = new Energy(100);
            Organism o1;
            try
            {
                o1 = new OrganismBuilder(initialEnergy).Build();
                Assert.Fail("Expected an InvalidOperationException to be thrown");
            }
            catch (InvalidOperationException exception)
            {
                Assert.IsTrue(exception.Message == "Argument can not be null.");
            }
        }

        [TestMethod]
        public void testFoodBuilder()
        {
            Food f1 = new FoodBuilder().Build();
            Assert.AreNotEqual(new Energy(10), f1.Energy);
            Assert.AreEqual(new Energy(100), f1.Energy);
        }
    }
}
