﻿using csharp_oop19_simulator.Ceredi_Simone;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Foods;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Giulianelli_Andrea;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Ceredi_Simone
{
    [TestClass]
    public class EnvironmentTest
    {
        private IEnvironmentFactory environmentFactory;
        private IOrganismBuilder organismBuilder;
        private IFoodBuilder foodBuilder;
        private IAdvancendEnvironment environment;
        
        [TestInitialize]
        public void TestInitialize()
        {
            environmentFactory = new EnvironmentFactory();
            organismBuilder = new OrganismBuilder(new Energy(100));
            foodBuilder = new FoodBuilder();
            foodBuilder.SetEnergy(new Energy(100));
            environment = environmentFactory.CreateAdvancedEnvironmnet(200, 200, 150, -2, new Temperature(20));
            organismBuilder.SetEnvironmentKnowledge(environment.OrganismEnvironmentHolder);
            organismBuilder.SetTrait(new Speed(1));
            // Adding 50 organisms to the environment.
            for (int i = 0; i < 50; i++)
            {
                environment.AddOrganism(organismBuilder.Build());
                environment.AddFood(foodBuilder.Build());
            }
        }

        [TestMethod]
        public void TestEnvironmentOrganismAdd() => Assert.AreEqual(50, environment.CurrentOrganismQuantity);

        [TestMethod]
        public void TestEnvironmentFoodAdd() => Assert.AreEqual(50, environment.CurrentFoodQuantity);

        [TestMethod]
        public void TestEnvironmentFoodClear()
        {
            environment.Clear();
            Assert.AreEqual(0, environment.CurrentFoodQuantity);
        }

        [TestMethod]
        public void TestEnvironmentOrganismRemove()
        {
            foreach (IOrganism organism in environment.GetOrganisms())
            {
                environment.RemoveOrganism(organism);
            }
            Assert.AreEqual(0, environment.CurrentOrganismQuantity);
        }

        [TestMethod]
        public void TestEnvironmentOrganismHolder()
        {
            Assert.AreEqual(20, environment.OrganismEnvironmentHolder.Temperature.Value);
        }
    }
}
