﻿using csharp_oop19_simulator.Ceredi_Simone;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Giulianelli_Andrea;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest.Giulianelli_Andrea
{
    [TestClass]
    public class TestTraits
    {
        //Valori iniziali trait.
        private const int SPEEDINITIAL = 5;
        private const int DIMENSIONINITIAL = 100;
        private const int CHILDRENINITIAL = 2;
        private const int FOODRADAR = 2;

        //Consumo di cibo aspettato
        private const int EXPTEMPSENSCONS = 2;
        private const int EXPFOODRADARCONS = 4;
        private const int EXPSPEEDCONS = 25;
        private const int EXPDIMCONS = 10;
        private const int EXPCHILDCONS = 40;

        private readonly Dictionary<TraitType, ITrait> traits = new Dictionary<TraitType, ITrait>();
        private IOrganism organism;

        /**
        * Initialise traits.
        */
        [TestInitialize]
        public void TestInitialize()
        { 
            this.traits.Add(TraitType.SPEED, new Speed(SPEEDINITIAL));
            this.traits.Add(TraitType.DIMENSION, new Dimension(DIMENSIONINITIAL));
            this.traits.Add(TraitType.CHILDRENQUANTITY, new ChildrenQuantity(CHILDRENINITIAL));
            this.traits.Add(TraitType.FOODRADAR, new FoodRadar(FOODRADAR));
            this.traits.Add(TraitType.TEMPERATURESENSIBILITY, new TemperatureSensibility());
            IAdvancendEnvironment environment = new EnvironmentFactory()
                .CreateAdvancedEnvironmnet(100, 100, 100, 0, new Temperature(10));
            IOrganismBuilder builder = new OrganismBuilder(new Energy(DIMENSIONINITIAL))
                .SetEnvironmentKnowledge(environment);
            //Set all the trait for the builder.
            this.traits.ToList()
                .ForEach((entrySet) => builder.SetTrait(entrySet.Value));
            this.organism = builder.Build();
        }

        [TestMethod]
        public void TestValues()
        {
            Assert.AreEqual(SPEEDINITIAL, this.traits[TraitType.SPEED].Value);
            Assert.AreEqual(DIMENSIONINITIAL, this.traits[TraitType.DIMENSION].Value);
            Assert.AreEqual(CHILDRENINITIAL, this.traits[TraitType.CHILDRENQUANTITY].Value);
            Assert.AreEqual(FOODRADAR, this.traits[TraitType.FOODRADAR].Value);
        }

        [TestMethod]
        public void TestFoodConsuption()
        {
            Assert.AreEqual(new Energy(EXPSPEEDCONS), this.traits[TraitType.SPEED].GetFoodConsumption(this.organism));
            Assert.AreEqual(new Energy(EXPDIMCONS), this.traits[TraitType.DIMENSION].GetFoodConsumption(this.organism));
            Assert.AreEqual(new Energy(EXPCHILDCONS), this.traits[TraitType.CHILDRENQUANTITY].GetFoodConsumption(this.organism));
            Assert.AreEqual(new Energy(EXPFOODRADARCONS), this.traits[TraitType.FOODRADAR].GetFoodConsumption(this.organism));
            Assert.AreEqual(new Energy(EXPTEMPSENSCONS), this.traits[TraitType.TEMPERATURESENSIBILITY].GetFoodConsumption(this.organism));
        }
    }
}
