﻿using csharp_oop19_simulator.Ceredi_Simone;
using csharp_oop19_simulator.Ceredi_Simone.Position;
using csharp_oop19_simulator.Conti_Alessio.Entity;
using csharp_oop19_simulator.Conti_Alessio.Entity.Organisms;
using csharp_oop19_simulator.Giulianelli_Andrea;
using csharp_oop19_simulator.Giulianelli_Andrea.Factory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest.Giulianelli_Andrea
{
    [TestClass]
    public class TestMutatedFactory
    {
        //Valori iniziali trait.
        private const int SPEEDINITIAL = 5;
        private const int DIMENSIONINITIAL = 100;
        private const int CHILDRENINITIAL = 2;
        private const int FOODRADAR = 2;

        private IOrganism organism;
        private IMutatedOrganismFactory factory;

        /**
        * Initialise traits.
        */
        [TestInitialize]
        public void TestInitialize()
        {
            Dictionary<TraitType, ITrait> traits = new Dictionary<TraitType, ITrait>();
            traits.Add(TraitType.SPEED, new Speed(SPEEDINITIAL));
            traits.Add(TraitType.DIMENSION, new Dimension(DIMENSIONINITIAL));
            traits.Add(TraitType.CHILDRENQUANTITY, new ChildrenQuantity(CHILDRENINITIAL));
            traits.Add(TraitType.FOODRADAR, new FoodRadar(FOODRADAR));
            traits.Add(TraitType.TEMPERATURESENSIBILITY, new TemperatureSensibility());
            IAdvancendEnvironment environment = new EnvironmentFactory()
                .CreateAdvancedEnvironmnet(100, 100, 100, 0, new Temperature(10));
            IOrganismBuilder builder = new OrganismBuilder(new Energy(DIMENSIONINITIAL))
                .SetEnvironmentKnowledge(environment);
            //Set all the trait for the builder.
            traits.ToList()
                .ForEach((entrySet) => builder.SetTrait(entrySet.Value));
            this.organism = builder.Build();
            this.factory = new MutatedOrganismFactory();
        }

        /**
         * Test that dad and children are different organism.
         */
        [TestMethod]
        public void TestMutatedOrganism()
        {
            IOrganism children = this.factory.CreateMutated(this.organism);
            Assert.AreNotSame(children, this.organism);
        }
    }
}
